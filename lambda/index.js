// TODO: refactor everything
const Telegraf = require('telegraf');
const request = require('request');

const bot = new Telegraf(process.env.BOT_TOKEN);

bot.command('addhashtag', (ctx) => {
    const splittedMessage = ctx.update.message.text.split(' ');
    const hashtag = splittedMessage[1];
    if (!hashtag) {
        ctx.reply('Sunny is cloudy, u specified empty hashtag');
        return;
    }

    if (hashtag[0] !== '#') {
        ctx.reply('Sunny is cloudy, u specified smth but not a hashtag');
        return;
    }

    request({
        method: 'POST',
        url: `${process.env.ADDRESS}/addhashtag`,
        json: true,
        body: { hashtag }
    },
    (error, response, body) => {
        ctx.reply(body.hashtags.join('\n'));
    });
});

bot.command('rmhashtag', (ctx) => {
    const splittedMessage = ctx.update.message.text.split(' ');
    const hashtag = splittedMessage[1];
    if (!hashtag) {
        ctx.reply('Sunny is cloudy, u specified empty hashtag');
        return;
    }

    if (hashtag[0] !== '#') {
        ctx.reply('Sunny is cloudy, u specified smth but not a hashtag');
        return;
    }

    request({
        method: 'POST',
        url: `${process.env.ADDRESS}/rmhashtag`,
        json: true,
        body: { hashtag }
    },
    (error, response, body) => {
        ctx.reply(body.hashtags.join('\n'));
    });
});

bot.command('hashtags', (ctx) => {
    request(`${process.env.ADDRESS}/hashtags`, (error, response, body) => {
        ctx.reply(JSON.parse(body).hashtags.join('\n'));
    });
});

bot.command('info', (ctx) => {
    ctx.reply(`Feel free to participate at https://gitlab.com/alkostryukov/sesunnybot or create your own bot, 
  sun is shining and so are you!`);
});

bot.command('start', (ctx) => {
    ctx.reply('commands:\n/hashtags \n/addhashtag #sunnyisthebest\n/rmhashtag #hashtag\n/info');
});


/* AWS Lambda handler function */
exports.handler = (event, context, callback) => {
    const body = JSON.parse(event.body); // get data passed to us
    bot.handleUpdate(body); // make Telegraf process that data
    return callback(null, { // return something for webhook, so it doesn't try to send same stuff again
        statusCode: 200,
        body: '',
    });
};
